FROM openjdk:11-jre-slim
WORKDIR /app
ENV SPRING_PROFILE=h2
COPY target/assignment-*.jar app.jar
EXPOSE 8080
ENTRYPOINT java -jar -Dspring.profiles.active=${SPRING_PROFILE} app.jar



